import test from 'ava';
import toDot from './toDot';

const testCases = {
  'graph {rankdir=LR;"A"[label="A"];}': {nodes: {A: {label: "A"}}, links: []},
  'graph {rankdir=LR;"B"[label="B"];}': {nodes: {B: {label: "B"}}, links: []},
  'graph {rankdir=LR;"A"[label="A"];"B"[label="B"];"A"--"B";}': {nodes: {A: {label: "A"}, B: {label: "B"}}, links: [{from: "A", to: "B"}]},
};
Object.keys(testCases).forEach((k)=>test(k,t=>t.is(toDot(testCases[k]),k)));

//test('"--" invalid -> throw error',t=>t.throws(()=>sgraph.parse('--')));
