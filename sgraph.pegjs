{
  function m(...objs){ //merge
    return objs.reduce(merge,{});
  }
	function clone(json) {
		return JSON.parse(JSON.stringify(json))
	}
  function merge(acc, obj) {
    if(typeof acc !== "object") return clone(obj);
    if(!obj) return acc;
    if(obj.class && acc.class){
      let classes = {}
      acc.class.split(" ").forEach(c=>classes[c]=c);
      obj.class.split(" ").forEach(c=>classes[c]=c);
      obj.class = Object.keys(classes).join(' ');
    } ;
    let res = clone(acc);
    for (let key in obj) {
      if (typeof obj[key] === "object" && typeof res[key] !== 'undefined') {
        res[key] = merge(res[key], obj[key]);
      }
      else res[key] = obj[key];
    }
    return res;
  }

}

Expressions
  = e:Expression Comment? es:NextExpression* { return m(e, ...es); }
  / Space { return {}; }

NextExpression = Separator e:Expression Comment? { return e; }

Expression
  = Relation
  / Actor
  / ClassDefinition
  / Comment { return {}; }
  / Space { return {}; }

Relation
  = a:Actor l:Link b:Actor { return m(a,b,{links:[{...{from:Object.keys(a.nodes)[0],to:Object.keys(b.nodes)[0]},...l}]}); }

Actor
  = Space id:Id cl1:Classes? ql:QuotedLabel? cl2:Classes? as:ActorShaped? cl3:Classes? Space { return {nodes:{[id]: m(cl1,ql,cl2,as,cl3)}}; }

ClassDefinition
  = Space "." className:Id Space Separator? Space "{" Space Separator? Space cd:ClassDeclarations Space Separator? Space "}" Space { return {classes:{[className]: cd }}; }

ClassDeclarations
  = cd:ClassDeclaration Comment? ncd:NextClassDeclaration* { return m(cd, ...ncd); }

NextClassDeclaration = Separator cd:ClassDeclaration Comment? { return cd; }

ClassDeclaration
  = key:ClassProp KeyValueSeparator value:(ClassProp/QuotedText) { return {[key]:value}; }

ActorShaped
  = "(" aq:ActorQualification ")" { return aq; }
  / "((" aq:ActorQualification "))" { return {...{shape:"circle"},...aq}; }
  / "[" aq:ActorQualification "]" { return {...{shape:"box"},...aq}; }
  / "[[" aq:ActorQualification "]]" { return {...{shape:"square"},...aq}; }
  / "<" aq:ActorQualification ">" { return {...{shape:"flat-diamon"},...aq}; }
  / "<<" aq:ActorQualification ">>" { return {...{shape:"diamon"},...aq}; }

ActorQualification = Options

Option
  = Space key:Id Space "=" Space value:(Id/QuotedText) Space { return {[key]:value}; }
  / Space label:(Id/QuotedText) Space { return {label:label}; }
  / Space cl:Classes Space { return cl; }
NextOption = [,]? o:Option { return o; }
Options = no:NextOption* { return m(...no); }

Class = "." cl:Id { return {class:cl}; }
Classes = cl:Class+ { return m(...cl); }

Link
  = Space "-" [-]+ Space { return {}; }
  / Space "-" [-]* ">" Space { return {dir:"forward"}; }


Id = $[_0-9a-zA-Z]+
ClassProp = $[-_0-9a-zA-Z]+
QuotedLabel = label:QuotedText { return {label:label}; }
QuotedText = ["] txt:$[^"]+ ["] { return txt; }
Space = [ \t]* BlockComment? [ \t]*
Separator = Space [;\n\r]+ Space
KeyValueSeparator = (Space [=:] Space) / [ \t]+
Comment
  = Space BlockComment Space
  / Space "//" [^\n\r]*

BlockComment = "/*" ([^*]+/[*][^/])* "*/"

