function toMermaid(graph){
  let result=`graph LR;`;
  Object.keys(graph.nodes).forEach(id=>result+=`${id}(${graph.nodes[id].label});`);
  graph.links.forEach(l=>result+=`${l.from}---${l.to};`);
  return result;
}
module.exports = toMermaid;
