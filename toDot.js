function toDot(graph){
  let result=``;
  Object.keys(graph.nodes).forEach(id=>result+=`"${id}"[label="${graph.nodes[id].label}"];`);
  graph.links.forEach(l=>result+=`"${l.from}"--"${l.to}";`);
  return `graph {rankdir=LR;${result}}`;
}
module.exports = toDot;
