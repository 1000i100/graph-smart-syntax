import test from 'ava';
//import parse from './generated.sgraph';
const sgraph = require('./generated.sgraph');

const testCases = {
  ' ': {},
  ' A  ': {nodes: {A: {}}},
  'A': {nodes: {A: {}}},
  'B': {nodes: {B: {}}},

  '// line comment bla bla bla ': {},
  '/* block comment bla bla bla */': {},
  '/* multi\nline\rblock comment bla bla bla */': {},
  '/* block comment with *stars* */': {},
  'A // comment': {nodes: {A: {}}},
  'A /// line comment // bla; bla bla \nB': {nodes: {A: {},B: {}}},
  'A /// more than \n// one line \n// comment\nB': {nodes: {A: {},B: {}}},
  'A; /* a block comment*/ B': {nodes: {A: {},B: {}}},

  'A(toto)': {nodes: {A: {label: "toto"}}},
  'A("(o|o)")': {nodes: {A: {label: "(o|o)"}}},
  'A"(o|o)"': {nodes: {A: {label: "(o|o)"}}},
  'A[]': {nodes: {A: {shape:"box"}}},
  'A[toto]': {nodes: {A: {label: "toto",shape:"box"}}},
  'A[[toto]]': {nodes: {A: {label: "toto",shape:"square"}}},
  'A((toto))': {nodes: {A: {label: "toto",shape:"circle"}}},
  'A<toto>': {nodes: {A: {label: "toto",shape:"flat-diamon"}}},
  'A<shape=box>': {nodes: {A: {shape:"box"}}},
  'A<<toto>>': {nodes: {A: {label: "toto",shape:"diamon"}}},
  'A< shape = box ,label="(o|o)">': {nodes: {A: {label: "(o|o)",shape:"box"}}},
  'A< shape = box "(o|o)">': {nodes: {A: {label: "(o|o)",shape:"box"}}},
  '/*yes*/A</*you*/shape/*can*/=/*put*/box/*a*/"(o|o)"/*comment*/>/*here*/': {nodes: {A: {label: "(o|o)",shape:"box"}}},
  'A("(o|o)",toto=tata,titi=tutu)': {nodes: {A: {label: "(o|o)",toto:"tata",titi:"tutu"}}},
  'A"(o|o)"(toto=tata,titi=tutu)': {nodes: {A: {label: "(o|o)",toto:"tata",titi:"tutu"}}},

  'A.myClass': {nodes: {A: {class:"myClass"}}},
  'A.c.d.e': {nodes: {A: {class:"c d e"}}},
  'A.c.c': {nodes: {A: {class:"c"}}},
  'A"toto".c': {nodes: {A: {label: "toto",class:"c"}}},
  'A.c"toto"': {nodes: {A: {label: "toto",class:"c"}}},
  'A.c(toto)': {nodes: {A: {label: "toto",class:"c"}}},
  'A(toto).c': {nodes: {A: {label: "toto",class:"c"}}},
  'A(toto.c)': {nodes: {A: {label: "toto",class:"c"}}},
  'A(toto .c)': {nodes: {A: {label: "toto",class:"c"}}},
  'A(.c"toto")': {nodes: {A: {label: "toto",class:"c"}}},
  'A(.c toto)': {nodes: {A: {label: "toto",class:"c"}}},
  'A( .c toto)': {nodes: {A: {label: "toto",class:"c"}}},
  'A(.c, toto)': {nodes: {A: {label: "toto",class:"c"}}},
  'A.c(toto, .c)': {nodes: {A: {label: "toto",class:"c"}}},
  'A.c(toto, .d)': {nodes: {A: {label: "toto",class:"c d"}}},

  'A;B': {nodes: {A: {},B: {}}},
  'A ; B': {nodes: {A: {},B: {}}},
  'A\nB': {nodes: {A: {},B: {}}},
  'A.a(toto);A[].b': {nodes: {A: {label: "toto",shape:"box",class:"a b"}}},


  '.c{shape=box}': {classes: {c: {shape: "box"}}},
  '.c{shape:box}': {classes: {c: {shape: "box"}}},
  '.c{shape box}': {classes: {c: {shape: "box"}}},
  ' .c { shape = "box" } ': {classes: {c: {shape: "box"}}},
  '.c{background-color yellow}': {classes: {c: {"background-color": "yellow"}}},
  '.c{shape=box;color=red}': {classes: {c: {shape: "box",color:"red"}}},
  '.c\n{\nshape=box\ncolor=red\n}\n': {classes: {c: {shape: "box",color:"red"}}},

  'A--B': {nodes: {A: {}, B: {}}, links: [{from: "A", to: "B"}]},
  'A -- B': {nodes: {A: {}, B: {}}, links: [{from: "A", to: "B"}]},
  'A---B': {nodes: {A: {}, B: {}}, links: [{from: "A", to: "B"}]},
  'A->B': {nodes: {A: {}, B: {}}, links: [{from: "A", to: "B",dir:"forward"}]},

  //'A -- {B,C}': {nodes: {A: {}, B: {}, C: {}}, links: [{from: "A", to: "B"},{from: "A", to: "C"}]},
  //'A --.c/.d B': {nodes: {A: {}, B: {}}, links: [{from: "A", to: "B", class:"c"},{from: "A", to: "B", class:"d"}]},
  //'A -- B -- C': {nodes: {A: {}, B: {}, C: {}}, links: [{from: "A", to: "B"},{from: "B", to: "C"}]},
  //'A -- { B C } ': {nodes: {A: {}, B: {}, C: {}}, links: [{from: "A", to: "B"},{from: "A", to: "C"}]},
  //'{A,B} -- C': {nodes: {A: {}, B: {}, C: {}}, links: [{from: "A", to: "C"},{from: "B", to: "C"}]},
  //'{A,B} -- {C,D}': {nodes: {A: {}, B: {}, C: {}, D: {}}, links: [{from: "A", to: "C"},{from: "A", to: "D"},{from: "B", to: "C"},{from: "B", to: "D"}]},
  /*
  '{A,B} --.c.d/.e {C,D}': {
    nodes: {A: {}, B: {}, C: {}, D: {}},
    links: [
      {from: "A", to: "C", class:"c d"},
      {from: "A", to: "D", class:"c d"},
      {from: "B", to: "C", class:"c d"},
      {from: "B", to: "D", class:"c d"},
      {from: "A", to: "C", class:"e"},
      {from: "A", to: "D", class:"e"},
      {from: "B", to: "C", class:"e"},
      {from: "B", to: "D", class:"e"}
      ]
  },
   */


  '.c{shape=box}\nA.c--B': {classes: {c: {shape: "box"}},nodes: {A: {class:"c"}, B: {}}, links: [{from: "A", to: "B"}]},
};
Object.keys(testCases).forEach((k)=>test(k,t=>t.deepEqual(sgraph.parse(k),testCases[k])));

test('empty string',t=>t.deepEqual(sgraph.parse(''),{}));
test('"--" invalid -> throw error',t=>t.throws(()=>sgraph.parse('--')));
