import test from 'ava';
import toMermaid from './toMermaid';

const testCases = {
  'graph LR;A(A);': {nodes: {A: {label: "A"}}, links: []},
  'graph LR;B(B);': {nodes: {B: {label: "B"}}, links: []},
  'graph LR;A(A);B(B);A---B;': {nodes: {A: {label: "A"}, B: {label: "B"}}, links: [{from: "A", to: "B"}]},
};
Object.keys(testCases).forEach((k)=>test(k,t=>t.deepEqual(toMermaid(testCases[k]),k)));

//test('"--" invalid -> throw error',t=>t.throws(()=>sgraph.parse('--')));
